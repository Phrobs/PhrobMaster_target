#!/bin/bash
#
# Make some room.
test -z "MYHOSTNAME" && (echo "MYHOSTNAME needs to be set!" ; exit 1)
apt-get remove -y python3 supercollider xserver-common libreoffice*
#
# Ensure mysql-server is installed with a blank password.
#
export DEBIAN_FRONTEND=noninteractive
apt-get install -y build-essential vim mysql-server python2.7 python-flask nginx avahi-daemon git sudo rsync libnss-mdns virtualenv python-dev libmysqlclient-dev sqlite3 mosquitto tmux uwsgi uwsgi-plugin-python logrotate whois python-pip mosquitto-clients avahi-utils
# Create pmadmin account
if [ ! -d /home/pmadmin ]; then
	# Generate crypt(3) password string
	ENCRYPTED_PASSWORD=`mkpasswd -m sha-512 pm.admin1`
	# Set up default user
	LANG=C adduser --gecos "Raspberry PI user" --add_extra_groups --disabled-password pmadmin
	LANG=C usermod -a -G sudo -p "${ENCRYPTED_PASSWORD}" pmadmin
fi
(cd /usr/local/PhrobMaster/files && rsync -vaxH . /)
#  fixup perms
chown -R pmadmin:sudo ~pmadmin/.ssh
chown root:root /etc/sudoers
chmod 600 ~pmadmin/.ssh/*
chmod 600 /etc/ssh/sshd_config
chmod 440 /etc/sudoers
chown -R pmadmin:sudo /usr/local/PhrobMaster
#
# Get the rest of our stuff.
#
cd /usr/local/PhrobMaster
if [ -d "www" ]; then
	sudo -u pmadmin /bin/bash -c '(cd www && git pull)'
else
	sudo -u pmadmin /bin/bash -c 'git clone git@gitlab.com:Phrobs/www.git www'
fi
if [ -d "www/snap" ]; then
	sudo -u pmadmin /bin/bash -c '(cd www/snap && git pull)'
else
	sudo -u pmadmin /bin/bash -c 'git clone git@gitlab.com:Phrobs/Snap.git www/snap'
fi
#
##echo "### Mysql Root password is blank for now ###"
##mysqladmin -u root -p create phrobs
##echo "grant usage on *.* to phrob@localhost identified by 'phrob-nuts'" | mysql -u root -p 
##echo "grant all privileges on phrobs.* to phrob@localhost" | mysql -u root -p 
##mysqladmin -u root -p flush-privileges
##mysqladmin -u root -p password phrob.master
#
# Now setup www
#
##cd /usr/local/PhrobMaster/www
##virtualenv .venv
##source .venv/bin/activate
##pip install -r requirements/dev.txt
##python manage.py db init
##python manage.py db migrate
##python manage.py db upgrade
#
# Now grab our backend stuff
#
sudo -u pmadmin /bin/bash -c 'git clone git@gitlab.com:Phrobs/Servers.git Backend'
cd /usr/local/PhrobMaster/Backend
virtualenv .venv
.venv/bin/pip install -r req.txt
systemctl enable uwsgi
rm -f phrobmaster.db
sqlite3 phrobmaster.db <<FOO
CREATE TABLE config (hostname text, phrob_server_port int, cname text);
insert into config values ("$MYHOSTNAME",5000,"$MYHOSTNAME");
FOO
sudo chown pmadmin:pmadmin phrobmaster.db
systemctl enable PhrobBackend
